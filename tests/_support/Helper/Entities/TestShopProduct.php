<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace Helper\Entities;

use RobotE13\ShoppingCart\Entities\Sellable;

/**
 * Description of ShopProduct
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class TestShopProduct implements Sellable
{

    /**
     * @var string
     */
    private $skuNumber;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $measureUnit;

    /**
     *
     * @var float
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var array
     */
    private $attributes;

    public function __construct(string $skuNumber, string $title, string $measureUnit, float $quantity, float $price, array $attributes)
    {
        $this->skuNumber = $skuNumber;
        $this->title = $title;
        $this->measureUnit = $measureUnit;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->attributes = $attributes;
    }

        public function getSkuNumber(): string
    {
        return $this->skuNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMeasureUnit(): string
    {
        return $this->measureUnit;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

}
