<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Helper\Builders;

use RobotE13\ShoppingCart\Entities\Item;

/**
 * Description of ItemBuilder
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class ItemBuilder
{

    private $uid;
    private $title;
    private $quantity;
    private $price;
    private $attributes = [];

    public function __construct()
    {
        $this->uid = "SKU-Test0001";
        $this->title = "Test product";
        $this->quantity = 1;
        $this->price = 11;
    }
    /**
     *
     * @param string $uid
     * @return self
     */
    public function withUid($uid)
    {
        $this->uid = $uid;
        return clone $this;
    }
    public function create(): Item
    {
        return new Item(
                $this->uid,
                $this->title,
                $this->quantity,
                $this->price,
                $this->attributes
        );
    }

}
