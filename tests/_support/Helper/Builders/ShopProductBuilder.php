<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace Helper\Builders;

/**
 * Description of ShopProductBuilder
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class ShopProductBuilder
{

    /**
     * @var string
     */
    private $skuNumber;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $measureUnit;

    /**
     *
     * @var float
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var array
     */
    private $attributes;

    public function __construct()
    {
        $this->skuNumber = 'Test-product-0001';
        $this->title = 'TestProduct';
        $this->measureUnit = 'pcs';
        $this->quantity = 1;
        $this->price = 10.75;
        $this->attributes = ['preview' => '/test.jpg'];
    }

    public function withUid($skuNumber): self
    {
        $this->skuNumber = $skuNumber;
        return clone $this;
    }

    public function withQuantity($quantity): self
    {
        $this->quantity = $quantity;
        return clone $this;
    }

    public function create(): \RobotE13\ShoppingCart\Entities\Sellable
    {
        return new \Helper\Entities\TestShopProduct(
                $this->skuNumber,
                $this->title,
                $this->measureUnit,
                $this->quantity,
                $this->price,
                $this->attributes
        );
    }

}
