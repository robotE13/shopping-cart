<?php

namespace Services;

use League\Tactician\CommandBus;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use RobotE13\ShoppingCart\Repositories\{
    InMemoryCart,
    NotFoundException
};
use RobotE13\ShoppingCart\Services\Cart\{
    AddItem\AddItem,
    AddItem\AddItemHandler,
    Get\GetCart,
    Get\GetCartHandler
};

class CartCommandsTest extends \Codeception\Test\Unit
{

    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var CommandBus
     * @specify
     */
    protected $commandBus;

    /**
     * @var InMemoryCart
     */
    protected $cartRepository;

    /**
     * @var \Helper\Builders\ShopProductBuilder
     */
    protected $shopProductBuilder;

    protected function _before()
    {
        $this->shopProductBuilder = new \Helper\Builders\ShopProductBuilder();
        $this->cartRepository = new InMemoryCart();
    }

    protected function _after()
    {
        $this->cartRepository = new InMemoryCart();
    }

    // tests
    public function testSuccessfulItemAdd()
    {
        $this->commandBus = $this->createCommandBus($this->cartRepository);

        $this->specify('Add items when cart not exist', function() {
            $command = new AddItem([
                $this->shopProductBuilder->create(),
                $this->shopProductBuilder->withUid('Test002')->withQuantity(2)->create()
            ]);
            $cart = $this->commandBus->handle($command);
            expect('Команда вернула объект типа cart', $cart)
                    ->isInstanceOf(\RobotE13\ShoppingCart\Entities\Cart::class);
        });
        
        $this->specify('Get command returns created cart', function() {
            $command = new GetCart();
            $cart = $this->commandBus->handle($command);
            expect('В хранилище есть корзина с двумя добавленными позициями', $cart->getItems())
                    ->count(2);
//            $this->cartRepository->remove();//нужна очистка репозитория, так как не получается задать его в specify
        });
    }

    public function testGetCommandFailed()
    {
        $this->commandBus = $this->createCommandBus($this->cartRepository);
        $this->specify('Get cart command throws exception if the cart was not created previously.', function() {
            $command = new GetCart();
            expect('Cart not exist', fn() => $this->commandBus->handle($command))
                    ->throws(NotFoundException::class);
        });
    }

    private function createCommandBus(&$cartRepository)
    {
        $locator = new InMemoryLocator();
        $locator->addHandler(new AddItemHandler($cartRepository), AddItem::class);
        $locator->addHandler(new GetCartHandler($cartRepository), GetCart::class);
        return new CommandBus([
            new CommandHandlerMiddleware(
                    new ClassNameExtractor(),
                    $locator,
                    new HandleInflector()
            )
        ]);
    }

}
