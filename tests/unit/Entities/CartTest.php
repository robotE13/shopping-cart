<?php

namespace Entities;

class CartTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *
     * @var \Helper\Builders\ItemBuilder;
     */
    protected $builder;

    protected function _before()
    {
        $this->builder = new \Helper\Builders\ItemBuilder();
    }

    public function testSuccessCreate()
    {

        $items = [
            $this->builder->create(),
            $this->builder->withUid('NEW-SKU')->create()
        ];
        $cart = new \RobotE13\ShoppingCart\Entities\Cart($items);

        expect("2 items are in cart", $cart->getItems())->count(2);
        expect("Subtotal for 2 items", $cart->getAmount())->equals('22.00');
        expect("Item with uid `NEW-SKU` in cart", $cart->itemExist('NEW-SKU'))->true();
        expect("Item with uid `NOT-EXIST` not presemt in cart", $cart->itemExist('NOT-EXIST'))->false();
        $cart->add($this->builder->create());
        expect("Subtotal for 3 items", $cart->getAmount())->equals('33.00');
        expect("Subtotal tax for 3 items", $cart->getTaxAmount())->equals('1.57');
        expect("Subtotal netto w/o tax for 3 items", $cart->getNettoAmount())->equals('31.43');
    }

    public function testSerialize()
    {
        $items = [
            $this->builder->create(),
            $this->builder->withUid('NEW-SKU')->create()
        ];
        $cart = new \RobotE13\ShoppingCart\Entities\Cart($items);
        $serializer = new \RobotE13\ShoppingCart\Services\Cart\Serializer($cart);
        $result = $serializer->jsonSerialize();
        expect('Result has `items`', $result)->hasKey('items');
        expect('Result has `subtotal`', $result)->hasKey('subtotal');
        expect('`Items` contain 2 items', $result['items'])->count(2);
        expect('Subtotal: 21 for 2 items', $result['subtotal'])->equals('22.00');
        expect('Success json_encode', json_encode($result))->stringContainsString('NEW-SKU');
    }

}
