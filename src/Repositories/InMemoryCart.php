<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Repositories;

use RobotE13\ShoppingCart\Entities\Cart;

/**
 * Description of InMemoryCart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class InMemoryCart implements CartRepository
{
    /**
     *
     * @var Cart
     */
    private $cart;

    public function get(): \RobotE13\ShoppingCart\Entities\Cart
    {
        if(!isset($this->cart))
        {
            throw new NotFoundException('Cart not exist.');
        }
        return clone $this->cart;
    }

    public function put(Cart $cart)
    {
        $this->cart = clone $cart;
    }

    public function remove(): \RobotE13\ShoppingCart\Entities\Cart
    {
        $cart = clone $this->cart;
        unset ($this->cart);
        return $cart;
    }

}
