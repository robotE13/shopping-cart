<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Repositories;

/**
 * Description of NotFoundException
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class NotFoundException extends \DomainException
{
    //put your code here
}
