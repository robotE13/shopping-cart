<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RobotE13\ShoppingCart\Repositories;

use RobotE13\ShoppingCart\Entities\Cart;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface CartRepository
{

    public function put(Cart $cart);

    /**
     *
     * @return Cart
     * @throws NotFoundException
     */
    public function get(): Cart;

    public function remove(): Cart;
}
