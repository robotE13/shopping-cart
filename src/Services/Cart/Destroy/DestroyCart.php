<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\Destroy;

/**
 * Description of DestroyCart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class DestroyCart
{
}
