<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart;

use RobotE13\ShoppingCart\Entities\{
    Cart,
    Item
};

/**
 * Description of Serializer
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Serializer implements \JsonSerializable
{

    /**
     * @var Cart
     */
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function jsonSerialize()
    {
//        $result['items'] = array_map(
//                fn($item) => $this->itemHydrator->extract($item),
//                $this->cart->getItems()
//        );
        $result['items'] = array_map(function($item) {
            /* @var $item Item */
            return[
                'uid' => $item->getUid(),
                'title' => $item->getTitle(),
                'quantity' => $item->getQuantity(),
                'price' => $item->getPrice(),
                'attributes' => $item->getAttributes(),
            ];
        }, $this->cart->getItems());
        $result['subtotal'] = $this->cart->getAmount();
        $result['subtotalTax'] = $this->cart->getTaxAmount();
        $result['subtotalNetto'] = $this->cart->getNettoAmount();
        return $result;
    }

}
