<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\Get;

use RobotE13\ShoppingCart\Entities\Cart;
use RobotE13\ShoppingCart\Repositories\CartRepository;

/**
 * Description of GetCartHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class GetCartHandler
{

    /**
     * @var CartRepository
     */
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle(GetCart $command):Cart
    {
        return $this->cartRepository->get();
    }
}
