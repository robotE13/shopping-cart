<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\RemoveItem;

use RobotE13\ShoppingCart\Repositories\CartRepository;

/**
 * Description of UpdateCartHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class RemoveItemHandler
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle(RemoveItem $command)
    {
        $cart = $this->cartRepository->get();
        $cart->remove($command->getUid());
        $this->cartRepository->put($cart);
        return clone $cart;
    }

}
