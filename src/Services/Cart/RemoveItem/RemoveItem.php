<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\RemoveItem;

/**
 * Description of UpdateCart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class RemoveItem
{

    private $uid;

    public function __construct(string $uid)
    {
        $this->uid = $uid;
    }

    public function getUid()
    {
        return $this->uid;
    }

}
