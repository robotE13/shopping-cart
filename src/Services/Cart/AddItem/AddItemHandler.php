<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\AddItem;

use RobotE13\ShoppingCart\Entities\Cart;
use RobotE13\ShoppingCart\Repositories\CartRepository;

/**
 * Description of CreateCartHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class AddItemHandler
{

    /**
     * @var CartRepository
     */
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle(AddItem $command): Cart
    {
        try {
            /* @var $item \RobotE13\ShoppingCart\Entities\Item */
            $cart = $this->cartRepository->get();
            array_walk($command->getItems(), fn($item) => $cart->put($item));
        } catch (\RobotE13\ShoppingCart\Repositories\NotFoundException $exc) {
            $cart = new Cart($command->getItems());
        }

        $this->cartRepository->put($cart);
        return $cart;
    }

}
