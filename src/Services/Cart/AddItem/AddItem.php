<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Services\Cart\AddItem;

use Webmozart\Assert\Assert;
use RobotE13\ShoppingCart\Entities\Item;
use RobotE13\ShoppingCart\Entities\Sellable;

/**
 * Description of CreateCart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class AddItem
{

    /**
     *
     * @var Item[]
     */
    protected $items;

    /**
     * Массив добавляемых в корзину товаров.
     * @param Sellable[] $items
     */
    public function __construct(array $items)
    {
        $this->items = array_map(fn($item) => $this->buildItem($item), $items);
    }

    /**
     *
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    private function buildItem(Sellable $product): Item
    {
        return new Item(
                $product->getSkuNumber(),
                $product->getTitle(),
                $product->getQuantity(),
                $product->getPrice(),
                $product->getAttributes()
        );
    }

}
