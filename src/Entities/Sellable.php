<?php

/*
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Entities;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface Sellable
{

    public function getSkuNumber(): string;

    public function getTitle(): string;

    public function getMeasureUnit(): string;

    public function getPrice(): float;

    public function getQuantity(): float;

    /**
     * Additional attributes for cart item like 'slug' or 'url_to_preview'.
     * @return array key=>value pairs
     */
    public function getAttributes(): array;
}
