<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Entities;

use Webmozart\Assert\Assert;

/**
 * Description of Item
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Item
{

    /**
     * @var string идентификатор товара. Обычно SKU number
     */
    private $uid;

    /**
     * @var string
     */
    private $title;

    /**
     * @var float
     */
    private $quantity;

    /**
     * @var float
     */
    private $price;

    /**
     * @var array additional attributes for cart item like 'slug' or 'url_to_preview' key=>value pairs.
     */
    private $attributes = [];

    public function __construct(string $uid, string $title, float $quantity, float $price, array $attributes = [])
    {
        Assert::allScalar($attributes);
        $this->uid = $uid;
        $this->title = $title;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->attributes = $attributes;
    }

    /**
     * Fixed Twig behavior on access to dynamic attribute via __get() magic method.
     * @link https://twig.symfony.com/doc/3.x/templates.html variables section
     * @param type $key
     * @return boolean
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    public function __get($name)
    {
        if(isset($this->attributes[$name]))
        {
            return $this->attributes[$name];
        }
        throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function getPrice(): string
    {
        return sprintf("%.2f", $this->price);
    }

    public function getCost(): string
    {
        return sprintf("%.2f", bcmul($this->getPrice(), $this->getQuantity(), 2));
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getAttributesNames()
    {
        return array_keys($this->attributes);
    }

    public function add(float $addedQuantity)
    {
        $this->quantity += $addedQuantity;
    }

    public function updateQuantity(float $newQuantity)
    {
        Assert::greaterThan($newQuantity, 0);
        $this->quantity = $newQuantity;
    }

    public function updatePrice(float $newPrice)
    {
        $this->price = $newPrice;
    }

}
