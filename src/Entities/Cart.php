<?php

/**
 * This file is part of the shopping-cart.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package shopping-cart
 */

namespace RobotE13\ShoppingCart\Entities;

use Webmozart\Assert\Assert;

/**
 * Description of Cart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Cart
{

    /**
     * @var Item[]
     */
    private $items = [];
    private $tax = 0.05;
    private $discounts;

    public function __construct(array $items)
    {
        array_walk($items, fn($item) => $this->add($item));
    }

    public function add(Item $item)
    {
        $uid = $item->getUid();
        if(!$this->itemExist($item->getUid()))
        {
            $this->items[$uid] = $item;
        } else
        {
            $this->items[$uid]->add($item->getQuantity());
            $this->items[$uid]->updatePrice($item->getPrice());
        }
    }

    /**
     * Put item to the cart.
     * Replace exist cart item.
     * @param Item $item
     */
    public function put(Item $item)
    {
        $uid = $item->getUid();
        $this->items[$uid] = $item;
    }

    public function updateQuantity($uid, $quantity)
    {
        if($this->itemExist($uid))
        {
            Assert::greaterThan($quantity, 0);
            $this->items[$uid]->updateQuantity($quantity);
            return clone $this;
        }
        throw new \DomainException("Product with uid `{$uid}` not in the cart");
    }

    public function remove($uid)
    {
        if($this->itemExist($uid))
        {
            unset($this->items[$uid]);
            return clone $this;
        }

        throw new \DomainException("Product with uid `{$uid}` not in the cart");
    }

    /**
     * Total cost of cart.
     * Sum without delivery.
     * @return string
     */
    public function getAmount(): string
    {
        return sprintf("%.2f", \array_reduce($this->items, fn($prev, $next) => $prev = bcadd($prev, bcmul($next->getPrice(), $next->getQuantity(), 2), 2), 0));
    }

    /**
     * Subtotal tax sum of cart.
     * Sum without delivery.
     * @return string
     */
    public function getTaxAmount(): string
    {
        return sprintf("%.2f", bcsub($this->getAmount(), $this->getNettoAmount(), 2));
    }

    /**
     * Subtotal of cart without tax.
     * Sum without delivery.
     * @return string
     */
    public function getNettoAmount(): string
    {
        return sprintf("%.2f", round(bcdiv($this->getAmount(), $this->tax + 1, 4),2));
    }

    public function getItems()
    {
        return $this->items;
    }

    public function itemExist($uid): bool
    {
        return key_exists($uid, $this->items);
    }

}
